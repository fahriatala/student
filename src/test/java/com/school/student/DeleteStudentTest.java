package com.school.student;

import com.school.student.model.entity.Student;
import com.school.student.repository.StudentRepository;
import com.school.student.service.StudentServiceImpl;
import com.shared.library.DataNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class DeleteStudentTest {
    @Mock
    private StudentRepository studentRepository;

    @InjectMocks
    private StudentServiceImpl studentService;

    @Captor
    private ArgumentCaptor<Student> studentArgumentCaptor;

    public DeleteStudentTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void deleteStudent_success() {
        Student existingStudent = new Student();
        existingStudent.setId(1L);

        when(studentRepository.findById(any())).thenReturn(Optional.of(existingStudent));
        studentService.deleteStudent(1L);

        Mockito.verify(studentRepository).save(studentArgumentCaptor.capture());
        Student student = studentArgumentCaptor.getValue();
        Assertions.assertNotNull(student.getDeletedAt());
    }

    @Test
    void deleteStudent_notFound() {
        when(studentRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(DataNotFoundException.class, () -> {
            studentService.deleteStudent(1L);
        });
    }
}
