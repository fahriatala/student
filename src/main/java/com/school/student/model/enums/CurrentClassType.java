package com.school.student.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Getter
@RequiredArgsConstructor
public enum CurrentClassType {
    X("1"),
    XI_SCIENCE("2"),
    XI_SOCIAL("3"),
    XII_SCIENCE("4"),
    XII_SOCIAL("4");

    private final String value;
    private static final Map<String, CurrentClassType> VALUE_MAP = new HashMap<>();

    static {
        for (CurrentClassType status : CurrentClassType.values()) {
            VALUE_MAP.put(status.getValue(), status);
        }
    }

    public static CurrentClassType fromValue(String value) {
        return VALUE_MAP.get(value.replace("_", " "));
    }

    public static boolean isValid(String value) {
        return VALUE_MAP.containsKey(value);
    }
}