package com.school.student.service;

import com.school.student.model.entity.Student;
import com.school.student.model.enums.CurrentClassType;
import com.school.student.model.request.StudentRequest;
import com.school.student.model.response.StudentResponse;
import com.school.student.repository.StudentRepository;
import com.shared.library.AppException;
import com.shared.library.DataNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;
    private static final String StudentNotFoundText = "Student not found for id ";

    @Override
    public List<StudentResponse> getAllStudents() {
        return studentRepository.findAll().stream()
                .map(this::convertToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public StudentResponse getStudentById(Long id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(StudentNotFoundText + id));

        return convertToResponse(student);
    }

    @Override
    public void insertStudent(StudentRequest request) {
        if (!CurrentClassType.isValid(request.getCurrentClass())) {
            throw new AppException("Invalid Current Class");
        }

        Student student = new Student();
        student.setFullName(request.getFullName());
        student.setEmail(request.getEmail());
        student.setPhoneNumber(request.getPhoneNumber());
        student.setAddress(request.getAddress());
        student.setDateOfBirth(request.getDateOfBirth());
        student.setPlaceOfBirth(request.getPlaceOfBirth());
        student.setPreviousSchool(request.getPreviousSchool());
        student.setCurrentClass(request.getCurrentClass());

        studentRepository.save(student);
    }

    @Override
    public void updateStudent(Long id, StudentRequest request) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(StudentNotFoundText + id));

        String currentClassRequest = request.getCurrentClass();
        if (currentClassRequest != null && !CurrentClassType.isValid(currentClassRequest)) {
            throw new AppException("Invalid Current Class");
        }

        student.setFullName(request.getFullName() != null ? request.getFullName() : student.getFullName());
        student.setEmail(request.getEmail() != null ? request.getEmail() : student.getEmail());
        student.setPhoneNumber(request.getPhoneNumber() != null ? request.getPhoneNumber() : student.getPhoneNumber());
        student.setAddress(request.getAddress() != null ? request.getAddress() : student.getAddress());
        student.setDateOfBirth(request.getDateOfBirth() != null ? request.getDateOfBirth() : student.getDateOfBirth());
        student.setPlaceOfBirth(request.getPlaceOfBirth() != null ? request.getPlaceOfBirth() : student.getPlaceOfBirth());
        student.setPreviousSchool(request.getPreviousSchool() != null ? request.getPreviousSchool() : student.getPreviousSchool());
        student.setCurrentClass(currentClassRequest != null ? currentClassRequest : student.getCurrentClass());

        studentRepository.save(student);
    }

    @Override
    public void deleteStudent(Long id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(StudentNotFoundText + id));
        student.setDeletedAt(LocalDateTime.now());

        studentRepository.save(student);
    }

    @Override
    public boolean checkStudentExistsById(long teacherId) {
        return studentRepository.existsById(teacherId);
    }

    private StudentResponse convertToResponse(Student student) {
        StudentResponse response = new StudentResponse();
        response.setId(student.getId());
        response.setFullName(student.getFullName());
        response.setEmail(student.getEmail());
        response.setPhoneNumber(student.getPhoneNumber());
        response.setAddress(student.getAddress());
        response.setDateOfBirth(student.getDateOfBirth());
        response.setPlaceOfBirth(student.getPlaceOfBirth());
        response.setPreviousSchool(student.getPreviousSchool());

        String currentClass = student.getCurrentClass();
        String currentClassText = (currentClass != null ? CurrentClassType.fromValue(currentClass).name() : null);
        response.setCurrentClass(currentClassText);

        return response;
    }
}
