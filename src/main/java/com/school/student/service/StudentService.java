package com.school.student.service;

import com.school.student.model.request.StudentRequest;
import com.school.student.model.response.StudentResponse;

import java.util.List;

public interface StudentService {
    List<StudentResponse> getAllStudents();
    StudentResponse getStudentById(Long id);
    void insertStudent(StudentRequest request);
    void updateStudent(Long id, StudentRequest request);
    void deleteStudent(Long id);
    boolean checkStudentExistsById(long teacherId);
}
